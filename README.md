# Making WordPress theme development great again
## Set up a WordPress theme dev environment in seconds
- Based on https://underscores.me/, sassified & WooCommerce-enabled
- Serves at ```localhost:33333```
- MySql access at ```localhost:33334```, access info on ```docker-compose.yaml```
- Replaces all theme slugs based on a custom theme name defined under ```./.setup/settings.yaml```
- Splits things up nicely:
    - Theme files under ```./dist/```
    - Sass source files under ```./src/sass/```
    - Raw DB files under ```./db/```

## Requirements
```
sudo apt install nodejs npm docker-compose
```

## Usage
```
git clone https://gitlab.com/nicklem1/wordpress-flash-setup.git
cd wordpress-flash-setup/ && npm run setup
sudo docker-compose up
```
Then navigate to ```localhost:33333```, finish the installation and activate the custom theme under ```Appearance/Themes```.

Sass compile & watch:
```
npm run sass
```

## Have fun!