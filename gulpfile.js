'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');

// Edit here
const autoprefixer_settings = {
    browsers: ['last 2 versions'],
    cascade: false
};
const options = {
    global: {
        glob: [
            './src/sass/**/*.scss',
            '!./src/sass/layout/*.scss',
            '!./src/sass/shop/*.scss',
            '!./src/sass/woocommerce.scss',
        ],
        task: 'style',
        src: './src/sass/style.scss',
        dest: './dist/'
    },
    woocommerce: {
        glob: [
            './src/variables-site/variables-site.scss',
            './src/mixins/mixins-master.scss',
            './src/sass/woocommerce.scss',
            './src/sass/shop/*.scss',
        ],
        task: 'woocommerce',
        src: './src/sass/woocommerce.scss',
        dest: './dist/'
    }
}
// End edit here

const allCompileTasks = [];
function allWatchTasks() {
    for (const o in options) {
        if (options.hasOwnProperty(o)) {
            const opt = options[o];
            gulp.watch(opt.glob, [opt.task]);
        }
    }
}

for (const o in options) {
    if (options.hasOwnProperty(o)) {
        const opt = options[o];
        allCompileTasks.push(opt.task);
        gulp.task(opt.task, function () {
            return gulp.src(opt.src)
                .pipe(sass().on('error', sass.logError))
                .pipe(autoprefixer(autoprefixer_settings))
                .pipe(gulp.dest(opt.dest));
        });
    }
}

gulp.task('sass', allCompileTasks);
gulp.task('watch', allWatchTasks);
