const assert = require('assert');
const install = require('../.setup/install');

describe('Setup', function () {
            it('slugifies allowed theme names', function () {
                assert.equal(install.slugifier('myTheme'), 'my-theme');
                assert.equal(install.slugifier('MyThemeName'), 'my-theme-name');
                assert.equal(install.slugifier('my-theme-name'), 'my-theme-name');
                assert.equal(install.slugifier('My_theme_Name'), 'my-theme-name');
                assert.equal(install.slugifier('Theme2018'), 'theme-2018');
                assert.equal(install.slugifier('Theme_2018'), 'theme-2018');
                assert.equal(install.slugifier('MY_THEME'), 'my-theme');
                assert.equal(install.slugifier('2018_MY-THEME'), '2018-my-theme');
                assert.equal(install.slugifier('2018MyTheme'), '2018-my-theme');
            });
            it('throws on unallowed theme names', function () {
                assert.throws(() => install.slugifier('Th&me'), Error, install.ERR_MSG.THEME_NAME);
                assert.throws(() => install.slugifier('una//owed'), Error, install.ERR_MSG.THEME_NAME);
                assert.throws(() => install.slugifier('$theme'), Error, install.ERR_MSG.THEME_NAME);
                assert.throws(() => install.slugifier('.123'), Error, install.ERR_MSG.THEME_NAME);
            });
            it('formats correctly all replace strings', function () {
                assert.deepEqual(install.formattedNames('MyThemeName'), {
                    textDomain: '\'my-theme-name\'',
                    functionPrefix: 'my_theme_name_',
                    cssTextDomain: 'Text Domain: my-theme-name',
                    docBlocks: ' My_Theme_Name',
                    prefixHandles: 'my-theme-name-',
                });
                assert.deepEqual(install.formattedNames('2018MyTheme'), {
                    textDomain: '\'2018-my-theme\'',
                    functionPrefix: '2018_my_theme_',
                    cssTextDomain: 'Text Domain: 2018-my-theme',
                    docBlocks: ' 2018_My_Theme',
                    prefixHandles: '2018-my-theme-',
                });
            });
});
