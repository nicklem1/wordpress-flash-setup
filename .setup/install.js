const fs = require('fs');
const rmdir = require('rmdir');
const unzip = require('unzip');
const yaml = require('js-yaml');
const download = require('download');
const stream = require('stream');

const TMP_DIR = '.tmp/';
const SRC_DIR = 'src/';
const DIST_DIR = 'dist/';
const SASS_DIR = 'sass/';
const MAKE_DIRS = [TMP_DIR, SRC_DIR];
const CLEANUP_DIRS = [DIST_DIR + 'layouts/', DIST_DIR + '.github/', TMP_DIR];
const SETUP_DIRS = [TMP_DIR, SRC_DIR, DIST_DIR].map((dir) => dir.replace(/\//, ''));
const ERR_MSG = {
  THEME_NAME: 'The theme name should contain only alphanumeric, _ and - characters.',
  SETUP_DIRS_PRESENT: 'Setup directories present. Please run setup only once.'
};

const REPLACE_REGEX = {
  textDomain: /\'\_s\'/g,
  functionPrefix: /\_s\_/g,
  cssTextDomain: /Text Domain\: \_s/g,
  docBlocks: / \_s/g,
  prefixHandles: /\_s\-/g,
};

const settings = yaml.safeLoad(fs.readFileSync('.setup/settings.yaml', 'utf8'));

function promisify(fn, ...p) {
  return new Promise((ok, no) => {
    return fn(...p, (err, data) => err ? no(err) : ok(data));
  });
}

function handleError(err) {
  console.log('\nERROR: ' + err.message + '\n');
}

function checkSetupFolders() {
  let setupFoldersPresent = fs.readdirSync('.').filter(function (dir) {
    return SETUP_DIRS.indexOf(dir) !== -1;
  });
  if (setupFoldersPresent.length)
    throw new Error(ERR_MSG.SETUP_DIRS_PRESENT);
  return;
}

function extractTheme(data) {
  return new Promise(function (resolve, reject) {
    try {
      let readBuffer = stream.Readable();
      let unzipper = unzip.Extract({
        path: TMP_DIR
      });
      unzipper.on('close', resolve);
      unzipper.on('error', reject);
      readBuffer._read = () => {};
      readBuffer.push(data);
      readBuffer.push(null);
      readBuffer.pipe(unzipper);
    } catch (e) {
      reject(e);
    }
  });
}

function slugifier(themeName) {
  if (!themeName.match(/^[a-z0-9_-]+$/i))
    throw new Error(ERR_MSG.THEME_NAME);
  themeName = themeName
    .replace(/([a-z])([A-Z0-9])|([0-9])([a-zA-Z])/g, (m) => `${m[0]}-${m[1]}`)
    .replace(/_/g, '-')
    .toLowerCase();
  return themeName;
}

function formattedNames(themeName) {
  themeName = slugifier(themeName);
  return {
    textDomain: '\'' + themeName + '\'',
    functionPrefix: themeName.replace(/\-/g, '_') + '_',
    cssTextDomain: 'Text Domain: ' + themeName,
    docBlocks: themeName
      .replace(/(^.)/, (m) => ` ${m}`.toUpperCase())
      .replace(/(-)([a-z0-9])/g, (m) => `_${m[1]}`.toUpperCase()),
    prefixHandles: themeName + '-',
  }
}

function stringReplace(fileName, oldRx, newStr) {
  try {
    fs.writeFileSync(
      fileName,
      String(fs.readFileSync(fileName)).replace(oldRx, newStr)
    );
  } catch (err) {
    handleError(err);
  }
}

function listAll(rootDir) {
  let list = fs.readdirSync(rootDir),
    files = [],
    path;
  rootDir.match(/\/$/) || (rootDir += '/');
  list.map(function (elem) {
      path = rootDir + elem;
    try {
      files = files.concat(listAll(path).map((f) => elem + '/' + f));
    } catch (_) {
      files.push(elem);
    }
  })
  return files;
}

function replaceStrings(rootDir) {
  return new Promise(function (resolve, reject) {
    try {
      let replaceNames = formattedNames(settings.themeName);
      let allFiles = listAll(rootDir);
      allFiles.map(function (path) {
        for (let name in replaceNames) {
          let oldRx = REPLACE_REGEX[name],
            newStr = replaceNames[name];
          stringReplace(rootDir + path, oldRx, newStr);
        }
      });
      resolve();
    } catch (err) {
      reject(err);
    }
  });
}

const ACTIONS = {
  MAKE_DIRS: () => Promise.all(MAKE_DIRS.map((dir) => promisify(fs.mkdir, dir))),
  DOWNLOAD_THEME: () => download(settings.themeUrl),
  EXTRACT_THEME: (data) => extractTheme(data),
  READ_TMP_DIR: () => promisify(fs.readdir, TMP_DIR),
  MOVE_THEME_TO_DIST: (dirs) => promisify(fs.rename, TMP_DIR + dirs[0], DIST_DIR),
  MOVE_SASS_TO_SRC: () => promisify(fs.rename, DIST_DIR + SASS_DIR, SRC_DIR + SASS_DIR),
  CLEANUP_DIRS: () => Promise.all(CLEANUP_DIRS.map((dir) => promisify(rmdir, dir))),
  REPLACE_THEME_STRINGS: () => replaceStrings(DIST_DIR),
  REPLACE_SASS_STRINGS: () => replaceStrings(SRC_DIR),
}

try {
  checkSetupFolders();
  ACTIONS.MAKE_DIRS()
    .then(ACTIONS.DOWNLOAD_THEME)
    .then(ACTIONS.EXTRACT_THEME)
    .then(ACTIONS.READ_TMP_DIR)
    .then(ACTIONS.MOVE_THEME_TO_DIST)
    .then(ACTIONS.MOVE_SASS_TO_SRC)
    .then(ACTIONS.CLEANUP_DIRS)
    .then(ACTIONS.REPLACE_THEME_STRINGS)
    .then(ACTIONS.REPLACE_SASS_STRINGS)
    .catch(handleError);
} catch (err) {
  handleError(err);
}

module.exports = {
  slugifier: slugifier,
  formattedNames: formattedNames,
  ERR_MSG: ERR_MSG,
};
